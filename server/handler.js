let connections = []
function connection(socket, io) {

    // stores the current group the notice board is connected
    let connection_group = ""
    
    // stores if the current connection is a notice board
    let is_board = false

    socket.on("register", (data) => {
        // this event should only be emitted by a notice board
        console.log(`client registered to ${data.group}`)
        socket.join(data.group)
        if (check_group_exist(connections, data.group) != true) {
            connections.push(data.group)
        }
        connection_group = data.group
        is_board = true
    })

    socket.on("disconnect", ()=>{
        console.log(`disconnected from ${connection_group}`)
        if (is_board && io.of("/").adapter.rooms[connection_group] == undefined) {
            connections = connections.filter((val, i, arr) => {
                return val != connection_group
            })
        }
    })

    socket.on("put", (data) => {
        // event emitted by the dashboard to set a new notice
        io.sockets.in(data.group).emit("new_notice", {message: data.message})
    })
    
    socket.on("get_info", () => {
        // returns connected groups
        socket.emit("info", connections)
    })
}


function check_group_exist(connections, group) {
    for (i = 0; i < connections.length; i++) {
        if (connections[i] == group) {
            return true
        } 
    }
    return false
}
module.exports = {
    connection
}