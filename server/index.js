const express = require("express")
const app = express()
const http = require("http")
const server = http.createServer(app)
const ejs = require("ejs")
const handler = require("./handler")

const { Server } = require("socket.io");
const io = new Server(server);


app.set("view engine", "ejs")

let notice_board_client_html = ""
let dash_board_html = ""
const fs = require("fs")

fs.readFile("./notice-client/index.ejs", 'utf8', (err, data) => {
    if (err) {
        console.error(err);
    
    }
    notice_board_client_html = data
})

fs.readFile("./dashboard/index.html", 'utf8', (err, data) => {
    if (err) {
        console.error(err);

    }
    dash_board_html = data
})

app.get("/", (_, res) => {
    res.send(dash_board_html)
})
app.get('/notice_board/:board_group/:board_id', (req, res) => {
    const group = req.params.board_group
    const id = req.params.board_id
    res.send(ejs.render(notice_board_client_html, {data: {id: id, group: group}}));
    console.log(`new board connected with id ${id} on ${group}`)
})

io.on("connection",(socket)=>{handler.connection(socket, io)})

server.listen(3000, () => {
    console.log("Listening on *:3000")
})

